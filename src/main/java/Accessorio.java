/**
 * Classe che descrive un accessorio del veicolo.
 */
public class Accessorio {
    private String nome;
    private double prezzo;

    /**
     * Costruttore che si occupa di creare un oggetto Accessorio con il nome e il prezzo indicato.
     * @param nome Stringa che rappresenta il nome dell'accessorio.
     * @param prezzo Double che rappresenta il prezzo dell'accessorio.
     */
    public Accessorio(String nome, double prezzo) {
        this.nome = nome;
        this.prezzo = prezzo;
    }

    /**
     * Metodo che ritorno il nome dell'accessorio.
     * @return La stringa che rappresenta il nome dell'oggetto accessorio.
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * Metodo che aggiorna il nome dell'accessorio.
     * @param nome Nuovo nome dell'accessorio.
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    /**
     * Metodo che ritorno il prezzo dell'accessorio.
     * @return La stringa che rappresenta il prezzo dell'oggetto accessorio.
     */
    public double getPrezzo() {
        return this.prezzo;
    }
    /**
     * Metodo che aggiorna il prezzo dell'accessorio.
     * @param prezzo Nuovo prezzo dell'accessorio.
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
}
