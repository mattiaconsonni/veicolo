import java.util.Random;

/**
 * Classe che modella un oggetto base di tipo Veicolo.
 */
public class VeicoloBase {
    private String tipoVeicolo;
    private int potenzaMotore;
    private Accessorio[] accessori;
    private int numAccessori;
    private double prezzo;


    /**
     * Costruttore che si occupa di definire un veicolo attraverso la sua tipologia, la sua potenza motore, il suo prezzo
     * e il suo numero massimo di accessori.
     * @param tipoVeicolo Stringa che rappresenta la tipologia del veicolo.
     * @param potenzaMotore Double che rappresenta la potenza del motore.
     * @param prezzo Double che rappresenta il prezzo del veicolo.
     * @param maxAccessori Intero che rappresenta il numero massimo di accessori.
     */
    public VeicoloBase(String tipoVeicolo, int potenzaMotore, double prezzo, int maxAccessori) {
        this.tipoVeicolo = tipoVeicolo;
        this.potenzaMotore = potenzaMotore;
        this.accessori = new Accessorio[maxAccessori];
        this.prezzo = prezzo;
    }

    /**
     * Metodo che aggiunge un accessorio se non è stato superato il limite massimo.
     * Se si raggiunge la capienza massima allora viene avvisato l'utente attraverso un stampa.
     * @param accessorio Oggetto di tip accessorio che si vuole inserire.
     */
    public void aggiungiAccessorio(Accessorio accessorio) {
        if (numAccessori != this.accessori.length) {
            this.accessori[numAccessori++] = accessorio;
            System.out.println("Accessorio aggiunto!");
       }
        else
            System.out.println("Numero di accessori massimo raggiunto.");
    }

    /**
     * Metodo che ritorno l'insieme di accessori del veicolo.
     * @return Una copia del array di accessori del veicolo.
     */
    public Accessorio[] getAccessori() {
        return this.accessori;
    }

    /**
     * Metodo che calcola la velocita massima del veicolo attraverso una formula arbitraria che utilizza la sua potenza.
     * @return La velocita massima del veicolo.
     */
    public double calcoloVelocitaMassima() {
        return (this.potenzaMotore * 10) / 5.;
    }

    /**
     * Metodo che stampa a schermo tutti gli accessori del veicolo.
     */
    public void stampaAccessori() {
        System.out.println("Lista di accessori: ");
        for (int i = 0; i < numAccessori; i++) {
            System.out.println("Accessorio n°" + i + ": " + this.accessori[i].getNome());
        }
    }

    /**
     * Metodo che calcolo il costo totale del veicolo sommato agli accessori.
     * @return Double che rappresenta il prezzo totale del veicolo.
     */
    public double costoTotale() {
        double somma = 0;
        for (int i = 0; i < numAccessori; i++) {
            somma += accessori[i].getPrezzo();
        }
        return (this.prezzo + somma);
    }

    /**
     * Metodo che stampa la quantità di litri necessaria per svolgere il viaggio coprendo la distanza indicata con il relativo consumo medio.
     * @param distanza Double che rappresenta la distanza da percorrere.
     * @param consumoMedio Double che rappresenta il consumo medio del veicolo.
     */
    public void simulaViaggio(double distanza, double consumoMedio) {
        System.out.println("Per compiere questo viaggio devi imbarcare: " + (distanza / this.calcoloVelocitaMassima()) * consumoMedio + " litri.");
    }

    /**
     * Metodo che stampa la velocità media del veicolo, ottenuta estraendo un numero random compreso tra 1 e 100.
     */
    public void velocitaMedia() {
        Random random = new Random();
        int velocitaMedia = random.nextInt(100) + 1;
        System.out.println("La velocità media è " + velocitaMedia);
    }
    /**
     * Metodo che ritorno il tipo di veicolo.
     * @return La stringa che rappresenta il tipo di veicolo.
     */
    public String getTipoVeicolo() {
        return this.tipoVeicolo;
    }
    /**
     * Metodo che aggiorna la tipologia del veicolo.
     * @param tipoVeicolo Nuova tipologia del veicolo.
     */
    public void setTipoVeicolo(String tipoVeicolo) {
        this.tipoVeicolo = tipoVeicolo;
    }
    /**
     * Metodo che ritorno la potenza di veicolo.
     * @return Il Double che rappresenta la potenza del veicolo.
     */
    public int getPotenzaMotore() {
        return this.potenzaMotore;
    }
    /**
     * Metodo che aggiorna la potenza del veicolo.
     * @param potenzaMotore Nuova potenza del veicolo.
     */
    public void setPotenzaMotore(int potenzaMotore) {
        this.potenzaMotore = potenzaMotore;
    }
    /**
     * Metodo che aggiorna gli accessori del veicolo.
     * @param accessori Nuovi accessori del veicolo.
     */
    public void setAccessori(Accessorio[] accessori) {
        this.accessori = accessori;
    }
    /**
     * Metodo che ritorno il numero corrente di accessori del veicolo.
     * @return L'Intero che rappresenta il numero corrente di accessori del veicolo.
     */
    public int getNumAccessori() {
        return this.numAccessori;
    }
    /**
     * Metodo che aggiorna il numero di accessori del veicolo.
     * @param numAccessori Nuovo numero di accessori del veicolo.
     */
    public void setNumAccessori(int numAccessori) {
        this.numAccessori = numAccessori;
    }
    /**
     * Metodo che ritorno il prezzo di veicolo.
     * @return Il Double che rappresenta il prezzo del veicolo.
     */
    public double getPrezzo() {
        return this.prezzo;
    }
    /**
     * Metodo che aggiorna il prezzo del veicolo.
     * @param prezzo Nuovo prezzo del veicolo.
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
}
