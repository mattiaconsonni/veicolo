import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Classe di Test della classe VeicoloBase
 */
class VeicoloBaseTest {

    private VeicoloBase veicoloBase;
    private Accessorio accessorio1;
    private Accessorio accessorio2;
    private Accessorio accessorio3;

    /**
     * Metodo di set up che viene eseguito prima di ogni test dove vengono istanziati un veicolo base e tre accessori.
     */
    @BeforeEach
    void setUp() {
        veicoloBase = new VeicoloBase("Automobile", 100, 30000, 5);
        accessorio1 = new Accessorio("Minigonna", 600);
        accessorio2 = new Accessorio("Spoiler", 400);
        accessorio3 = new Accessorio("Interni Sportivi", 300);
    }

    /**
     * Test che verifica il funzionamento del metodo calcolaVelocitaMassima.
     */
    @Test
    void testCalcolaVelocitaMassima() {
        assertEquals((veicoloBase.getPotenzaMotore() * 10) / 5., veicoloBase.calcoloVelocitaMassima());
    }

    /**
     * Test che verifica il funzionamento del metodo calcolaCostoTotale.
     */
    @Test
    void testCalcolaCostoTotale() {
        veicoloBase.aggiungiAccessorio(accessorio1);
        veicoloBase.aggiungiAccessorio(accessorio2);
        veicoloBase.aggiungiAccessorio(accessorio3);
        assertEquals(31300, veicoloBase.costoTotale());
    }
}